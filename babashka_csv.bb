(ns babashka-csv)

(require '[clojure.data.csv :as csv]
         '[clojure.java.io :as io]
         '[clojure.string :as str])

(def alph-vec ["A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"])
(def alph-to-num (loop [vec alph-vec
                        alph-map {}
                        n 1]
                   (if (empty? vec)
                     alph-map
                     (recur (rest vec)
                            (assoc alph-map (first vec) n)
                            (inc n)))))

(defn csv-num
  [coord]
  (if (string? coord)
       ((symbol coord) alph-to-num)
    (when (int? coord) coord)))

(defn read-csv
  [csv]
  (with-open [reader (io/reader csv)]
    (doall (csv/read-csv reader))))

(defn read-csv-lazy
  [csv-reader]
  (csv/read-csv csv-reader))

(defn get-row
  [csv row]
  (let [row-num (- (csv-num row) 1)]
    (nth csv row-num)))

(defn get-column
  [csv col]
  (let [col-num (- (csv-num col) 1)]
    (loop [csv-left csv
           cols []]
      (if (empty? csv-left)
        cols
        (recur (rest csv-left)
               (conj cols
                     (nth (first csv-left) col-num)))))))

(defn invert-csv
  "Get a list of all columns, essentially swapping rows and columns for easier column access"
  [csv]
  (let [col-num (count (first csv))]
    (loop [cur-col 1
           cols []]
      (if (> cur-col col-num)
        cols
        (recur (inc cur-col) (conj cols (get-column csv cur-col)))))))

(defn get-all-columns
  [csv]
  (invert-csv csv))

(defn get-cell
  [csv row col]
  (get-column [(get-row csv row)] col))

(defn get-col-by-header
  [csv header-term]
  (let [headers (first csv)]
    (loop [headers-left headers
           n 1]
      (when (seq headers-left)
        (if (str/includes? (first headers-left) header-term)
          [(get-column csv n) n]
          (recur (rest headers-left) (inc n)))))))

(defn get-cols-by-headers
  [csv header-terms]
  (filter #(some (fn [header] (str/includes? (first %) header)) header-terms) (get-all-columns csv)))

;; get-cols-by-headers makes the CSV cols x rows so we are putting it back to rows x cols
(defn filter-headers-csv
  [csv header-terms]
  (invert-csv (get-cols-by-headers csv header-terms)))

(defn write-csv
  [csv-name csv]
  (with-open [writer (io/writer csv-name)]
    (csv/write-csv writer csv)))
